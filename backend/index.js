import express from "express"
import { getCCA, getICF } from "./lib/scraper"
import db from "./lib/db"
import "./lib/cron"

// initialize cors
const cors = require("cors")

// setup express server
const app = express()

// use cors
app.use(cors())

// log out current data
console.info("Current data:", db.value())

// end point to initiate scrape
app.get("/scrape", async (req, res, next) => {
  console.info("REQUEST: ", req.headers, new Date(Date.now()))
  // if no request headers then foh
  if (!req.headers) return
  console.log("Scraping...")

  const [dataFetchPromise] = await Promise.all([
    getCCA(),
    // getICF()
  ])

  console.info("Scraping complete. Data updated")

  res.json(dataFetchPromise)
})

//end point for the front end
app.get("/data", async (req, res, next) => {
  console.info("Scraped data request: ", req.headers, new Date(Date.now()))
  // if no request headers then foh
  if (!req.headers) return

  // get scrapped data from local json db
  const data = db.value()

  //response with json
  res.json(data)
})

app.listen(3000, log => {
  console.info(`Scraper running on port http://localhost:3000`)
})
