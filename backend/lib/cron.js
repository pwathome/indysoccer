import cron from "node-cron"
import { getCCA } from "./scraper"

cron.schedule("0 * * * *", () => {
  getCCA()
  console.info("Cronjob ran!")
})