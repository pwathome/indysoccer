import low from "lowdb"
import FileSync from "lowdb/adapters/FileSync"

// DB setup
const adapter = new FileSync("db.json")
const db = low(adapter)

db.defaults({
  cca: [],
  icf: [],
  pickup: []
}).write()

export default db
