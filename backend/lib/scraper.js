import axios from "axios"
import * as cheerio from "cheerio"
import db from "./db"

// fetch HTML function
const fetchHTML = async (url) => {
	// Use axios to fetch the HTML. Curlies are used to "destructure" what we get back
	const { data: html } = await axios.get(url)
	console.info("HTML fetched!")
	return html
}

export async function getCCA(html) {
	html = await fetchHTML("https://www.ccasports.com/league/25748/schedule")
	// here we load the html into cheerio
	const $ = cheerio.load(html, {
		normalizeWhitespace: true,
		xmlMode: true
	})
	//CCA logic
	// extract the data we need
	console.info("Begin data extract from HTML")
	const ccAData = []
	const sessionTitle = $(".leagueTitle").html()
	$(".gameDate").map(function (day) {
		day = $(this)
		const gameDayObj = {
			date: day.find("h2").text().trim().toString(),
			firstGames: {
				time: day.find("tr td.gameTime").get(0) ?
					day.find("tr td.gameTime").get(0).children[0].data.trim().toString() : "N/A",
				fieldOne: {
					teamOne: day.find(".gameCellContent a").get(0) ?
						day.find(".gameCellContent a").get(0).children[0].data.trim().toString() : "N/A",
					teamTwo: day.find(".gameCellContent a").get(1) ?
						day.find(".gameCellContent a").get(1).children[0].data.trim().toString() : "N/A",
					score: {
						teamOne: day.find(".scoreDisplay").get(0) ?
							day.find(".scoreDisplay").get(0).children[0].data.trim().replace(/\D/g, "").toString() : "NA",
						teamTwo: day.find(".scoreDisplay").get(1) ?
							day.find(".scoreDisplay").get(1).children[0].data.trim().replace(/\D/g, "").toString().toString() : "N/A",
					}
				},
				fieldTwo: {
					teamOne: day.find(".gameCellContent a").get(2) ?
						day.find(".gameCellContent a").get(2).children[0].data.trim().toString() : "N/A",
					teamTwo: day.find(".gameCellContent a").get(3) ?
						day.find(".gameCellContent a").get(3).children[0].data.trim().toString() : "N/A",
					score: {
						teamOne: day.find(".scoreDisplay").get(2) ?
							day.find(".scoreDisplay").get(2).children[0].data.trim().replace(/\D/g, "").toString().toString() : "N/A",
						teamTwo: day.find(".scoreDisplay").get(3) ?
							day.find(".scoreDisplay").get(3).children[0].data.trim().replace(/\D/g, "").toString() : "N/A",
					}
				}
			},
			secondGames: {
				time: day.find("tr td.gameTime").get(1) ?
					day.find("tr td.gameTime").get(1).children[0].data.trim().toString() : "N/A",
				fieldOne: {
					teamOne: day.find(".gameCellContent a").get(4) ?
						day.find(".gameCellContent a").get(4).children[0].data.trim().toString() : "N/A",
					teamTwo: day.find(".gameCellContent a").get(5) ?
						day.find(".gameCellContent a").get(5).children[0].data.trim().toString() : "N/A",
					score: {
						teamOne: day.find(".scoreDisplay").get(4) ?
							day.find(".scoreDisplay").get(4).children[0].data.trim().replace(/\D/g, "").toString() : "N/A",
						teamTwo: day.find(".scoreDisplay").get(5) ?
							day.find(".scoreDisplay").get(5).children[0].data.trim().replace(/\D/g, "").toString() : "N/A",
					}
				},
				fieldTwo: {
					teamOne: day.find(".gameCellContent a").get(6) ?
						day.find(".gameCellContent a").get(6).children[0].data.trim().toString() : "N/A",
					teamTwo: day.find(".gameCellContent a").get(7) ?
						day.find(".gameCellContent a").get(7).children[0].data.trim().toString() : "N/A",
					score: {
						teamOne: day.find(".scoreDisplay").get(6) ?
							day.find(".scoreDisplay").get(6).children[0].data.trim().replace(/\D/g, "").toString() : "N/A",
						teamTwo: day.find(".scoreDisplay").get(7) ?
							day.find(".scoreDisplay").get(7).children[0].data.trim().replace(/\D/g, "").toString() : "N/A",
					}
				}
			}
		}
		if (gameDayObj) {
			ccAData.push(gameDayObj)
		}
	})

	if (ccAData.length > 0) {
		const data = {
			session: sessionTitle,
			schedule: ccAData,
			scrapeDate: new Date(Date.now())
		}
		if (data && data.sessionTitle !== null) {
			// drop db before pushing new data
			console.info("Database dropped!")
			db.get("cca").remove().write()
			// push new data to local database
			console.info("Pushing new CCA data...")
			db.get("cca").push({
				"sessionTitle": data.session,
				"data": data.schedule,
				"fetchDate": data.scrapeDate
			}).write()
			console.info("Database update complete!")
		}
		return data
	}
}

export async function getICF(html) {
	html = await fetchHTML("https://www.ccasports.com/league/25749/schedule")
	// here we load the html into cheerio
	const $ = cheerio.load(html, {
		normalizeWhitespace: true,
		xmlMode: true
	})
	//CCA logic
	// extract the data we need
	console.info("Begin data extract from HTML")
	const icfData = []
	const sessionTitle = ""
	$(".gameDate").map(function (day) {
		day = $(this)
		// const gameDayObj = {}
		// if (gameDayObj) {
		// 	icfData.push(gameDayObj)
		// }
	})

	if (icfData.length > 0) {
		const data = {
			session: sessionTitle,
			schedule: icfData,
			scrapeDate: new Date(Date.now())
		}
		return data
	}
	console.info("ICF HTML data extracted!")
}
