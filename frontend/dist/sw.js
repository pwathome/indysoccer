importScripts("precache-manifest.93c87fa623aa3da3066309149f71f919.js", "https://storage.googleapis.com/workbox-cdn/releases/4.3.1/workbox-sw.js");

console.log("⚙️ hello from service worker");

workbox.routing.registerRoute(
  /https:\/\/jsonplaceholder\.typicode\.com/,
  workbox.strategies.networkFirst()
)

// workbox.skipWating()

workbox.precaching.precacheAndRoute(self.__precacheManifest);
